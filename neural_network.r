library( neuralnet )
library( hydroGOF )
library( leaps )
library( arules )
library(caret)
library(boot)
library("QSARdata")



redwine <- read.csv("http://archive.ics.uci.edu/ml/machine-learning-databases/wine-quality/winequality-red.csv",header=TRUE,sep=";",dec=".")
whitewine <- read.csv("http://archive.ics.uci.edu/ml/machine-learning-databases/wine-quality/winequality-white.csv",header=TRUE,sep=";",dec=".")

# criar coluna para tipo de vinho (0,1), juntar dados
redwine
whitewine

summary(redwine)
summary(whitewine)




vr <- rep(0, nrow(redwine)) 
vw <- rep(1, nrow(whitewine)) 
redwine$wine_type <- vr
whitewine$wine_type <- vw
wines <- rbind(redwine,whitewine)

sum(unlist(lapply(lapply(wines, is.na), sum)))    # total de c�lulas vazias em todas as colunas. n�o existem c�lulas vazias nos datasets
unlist(lapply(wines,class))    #class de cada coluna no dataset (Integer, numeric, ...)

sum(wines$wine_type == 0)
nrow(redwine)
sum(wines$wine_type == 1)
nrow(whitewine)

pie(table(wines$quality))


hist(wines$residual.sugar)
hist(wines$quality)

boxplot(wines$alcohol ~ wines$quality)
boxplot(wines$quality ~ wines$wine_type)


cor(wines$residual.sugar, wines$density) # correla��o entre residual.sugar e density
cor(wines$quality, wines$volatile.acidity)
cor(wines$quality, wines$alcohol)
cor(wines$quality, wines$sulphates)
cor(wines$quality, wines$residual.sugar)

#an�lise / cria��o do modelo
"
usar redes neuronais (slide 87 do pdf de machine learning)
"

#coisas a fazer:

# - identificar topologia de rede adequada (na fun��o neuralnet: alterar valores do campo hidden)
# - selecionar regras/algoritmo de aprendizagem (na fun��o neuralnet: mudar algoritmo)




#normaliza��o dos dados 
maxs <- apply(wines, 2, max) 
mins <- apply(wines, 2, min)
scaledWines <- as.data.frame(scale(wines, center = mins, scale = maxs - mins))

colMeans(scaledWines)  
apply(scaledWines, 2, sd)

#criar sets de treino e teste
set.seed(12345)
ind <- sample(2, nrow(wines), replace=TRUE, prob=c(0.7, 0.3))
trainSet <- scaledWines[ind==1,]
testSet <- scaledWines[ind==2,]

lm.fit <- glm(quality~., data=trainSet)
pr.lm <- predict(lm.fit,testSet)
MSE.lm <- sum((pr.lm - testSet$quality)^2)/nrow(testSet)

funcDefault <- quality ~ residual.sugar + alcohol + fixed.acidity + volatile.acidity + citric.acid + chlorides + free.sulfur.dioxide + total.sulfur.dioxide + density + pH + sulphates

#diferentes algoritmos de sele��o
selecaoNV <- regsubsets(funcDefault,wines,nvmax=5)
summary(selecaoNV)

selecaoBack <- regsubsets(funcDefault,wines,method="backward")
summary(selecaoBack)

selecaoFor <- regsubsets(funcDefault,wines,method="forward")
summary(selecaoFor)

selecaoEx <- regsubsets(funcDefault,wines,method="exhaustive")
summary(selecaoEx)

selecaoSeq <- regsubsets(funcDefault,wines,method="seqrep")
summary(selecaoSeq)


#escolher quantas variaveis usar (n�o est� adaptada)
ctrl <- trainControl("cv", number = 5)
credCont <- train(funcSign, data = scaledWines, method="neuralnet", tuneLength=5, trControl= ctrl, stepmax = 50, rep = 10)
credCont





#control <- trainControl(method="repeatedcv", number=5, repeats=1)
# train the model
#model <- train(funcDefault, data = wines, method="neuralnet", 
#               algorithm = "backprop", learningrate = 0.25,act.fct = 'tanh',
#               tuneGrid = data.frame(layer1 = 1:10, layer2 = 1:10, layer3 = 1:10),threshold = 0.1, trControl=control)


#colunas mais significativas
funcSign <- quality ~ alcohol + volatile.acidity + sulphates


#criar rna
#algoritmos  'backprop', 'rprop+', 'rprop-', 'sag', e 'slr'
rnaquality <- neuralnet( funcSign, trainSet, hidden = c(4),algorithm = "rprop-", lifesign = "full", linear.output = FALSE, threshold = 0.1)

# desenhar rede neuronal
plot(rnaquality, rep = "best")


#n <- names(trainSet)
#f <- as.formula(paste("quality ~", paste(n[!n %in% "quality"], collapse = " + ")))
#nn <- neuralnet(f,data=trainSet,hidden=c(4,2), lifesign = "full", linear.output=T)



# definir variaveis de input para teste
teste.01 <- subset(testSet, select = c("alcohol", "volatile.acidity", "sulphates","residual.sugar"))

rnaquality.resultados <- compute(rnaquality, teste.01)


resultados <- data.frame(atual = testSet[,"quality"], previsao = rnaquality.resultados$net.result)


# imprimir resultados arredondados
resultados$previsao <- round(resultados$previsao, digits=0)

# calcular o RMSE
rmse(c(testSet[,"quality"]),c(resultados$previsao))


#cross validation e an�lise de performance
set.seed(200)
lm.fit <- glm(quality~.,data=scaledWines)
cv.glm(scaledWines,lm.fit,K=10)$delta[1]

set.seed(450)
cv.error <- NULL
k <- 10

library(plyr) 
pbar <- create_progress_bar('text')
pbar$init(k)

for(i in 1:k){
  index <- sample(1:nrow(scaledWines),round(0.9*nrow(scaledWines)))
  train.cv <- scaledWines[index,]
  test.cv <- scaledWines[-index,]
  
  
  
  nn <- neuralnet(funcSign,data=train.cv, algorithm = "rprop+",hidden=c(5,2), lifesign = "full", linear.output=T)
  
  pr.nn <- compute(nn,test.cv[,1:12])
  pr.nn <- pr.nn$net.result*(max(scaledWines$quality)-min(scaledWines$quality))+min(scaledWines$quality)
  
  test.cv.r <- (test.cv$quality)*(max(scaledWines$quality)-min(scaledWines$quality))+min(scaledWines$quality)
  
  MSE.nn <- sum((test.cv.r - pr.nn)^2)/nrow(test.cv)
  
  print(paste(MSE.lm,MSE.nn)) #comparar performance do da rede com a do modelo linear
  
  cv.error[i] <-  sqrt(MSE.nn)
  
  pbar$step()
}

mean(cv.error)

